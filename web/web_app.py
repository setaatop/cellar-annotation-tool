import datetime
import time
import uuid

from requests import Response
from flask import Flask, render_template, request, send_file
from flask_wtf import FlaskForm
import rdflib
from wtforms import FileField, TextAreaField

from flask_uploads import configure_uploads, DOCUMENTS, UploadSet
from logging.config import dictConfig
import logging

import pandas as pd


# import jsonify
from flask import jsonify
import json

from anytree import Node, RenderTree
from anytree.exporter import JsonExporter


# for logging
# https://stackoverflow.com/questions/51318988/why-flask-logger-does-not-log-in-docker-when-using-uwsgi-in-front
dictConfig(
    {
        "version": 1,
        "formatters": {
            "default": {
                "format": "[%(asctime)s] %(levelname)s in %(module)s: %(message)s",
            }
        },
        "handlers": {
            "wsgi": {"class": "logging.StreamHandler", "formatter": "default"}
        },
        "root": {"level": "DEBUG", "handlers": ["wsgi"]},
    }
)

logging.basicConfig(
    filename="record.log",
    level=logging.DEBUG,
    format=f"%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s",
)


#  FIX werkzeug bug by importing
# from werkzeug.utils import secure_filename
# from werkzeug.datastructures import  FileStorage
#  instead of
# from werkzeug import secure_filename, FileStorage
# go to flask_uploads.py and change the import statement
# FIXED by using Flask-Reuploads instead of Flask-Uploads
# https://stackoverflow.com/questions/61628503/flask-uploads-importerror-cannot-import-name-secure-filename
import os, sys


module_path = os.path.abspath(os.getcwd() + "/..")

if module_path not in sys.path:
    sys.path.append("/home/cellar_seta/")

from core.helpers import (
    config_helper,
    seta_helper,
    metadata_helper,
    sparql_op_helper,
    tika_helper,
)
from core.utils import clean

from core.document_classifier import classifier
from collections import defaultdict


web_app = Flask(__name__, template_folder="templates")

from apscheduler.schedulers.background import BackgroundScheduler


UPLOAD_FOLDER = "uploads/"
web_app.config["UPLOAD_FOLDER"] = UPLOAD_FOLDER
ALLOWED_EXTENSIONS = {"txt", "pdf", "png", "jpg", "jpeg", "gif"}


web_app.config["SECRET_KEY"] = "mysecretkey"
web_app.config["UPLOADED_FILES_DEST"] = "uploads/files"


DOCUMENTS = tuple("pdf doc docx txt".split())

files = UploadSet("files", DOCUMENTS)
# pdf = UploadSet("pdf", "pdf")

configure_uploads(web_app, files)


class MyForm(FlaskForm):
    file = FileField("file")
    text = TextAreaField("text")
    title = ""
    action = "/"


@web_app.before_first_request
def before_first_request():
    web_app.logger.debug("init SeTA...")
    try:
        seta_helper.get_similar_docs_by_content("Energy")
    except:
        web_app.logger.debug("Done ")


@web_app.route("/")
def index():
    return render_template("index.html")


@web_app.route("/sim_docs/", methods=["GET", "POST"])
def sim_docs():
    form = MyForm()
    form.action = "/sim_docs/"
    form.title = "Similar Documents"
    if form.validate_on_submit():
        if form.file.data:
            filename = files.save(form.file.data)
            full_path = os.path.join(web_app.config["UPLOADED_FILES_DEST"], filename)
            tika_helper.init()
            text = tika_helper.parse(full_path)
        else:
            text = form.text.data

        # sources = config_helper.app_config["sources"]
        sources = ["bookshop", "eurlex"]
        #         sources = ["eurlex", "bookshop"]
        print(sources)

        docs = seta_helper.get_similar_docs_by_content(text, 10, sources)

        # docs = seta_helper.get_similar_docs_by_content(text)

        return render_template("sim_docs.html", documents=docs["documents"])

    return render_template("upload_doc.html", form=form)


@web_app.route("/classify/", methods=["GET", "POST"])
def classify():

    form = MyForm()
    form.action = "/classify/"
    form.title = "Document Classifier"

    if form.validate_on_submit():

        if form.file.data:
            filename = files.save(form.file.data)
            full_path = os.path.join(web_app.config["UPLOADED_FILES_DEST"], filename)
            tika_helper.init()
            text = tika_helper.parse(full_path)

        else:
            text = form.text.data

        data_dir = config_helper.app_config["data_dir"]
        base_docs_dir = data_dir + "VDL/"

        seta_helper.reload()

        classifier.prepare_base_docs(base_docs_dir)

        base_docs = classifier.get_base_docs(base_docs_dir)
        classifier_ = classifier(seta_helper, base_docs)

        classes = classifier_.classify(text)

        return render_template("classify.html", classes=classes, form=form)

    return render_template("upload_doc.html", form=form)


@web_app.route("/metadata/", methods=["GET", "POST"], strict_slashes=False)
def metadata():

    form = MyForm()
    form.action = "/metadata/"
    form.title = "Metadata Annotator"

    if form.validate_on_submit():
        is_file = False
        if form.file.data:
            is_file = True
            filename = files.save(form.file.data)
            full_path = os.path.join(web_app.config["UPLOADED_FILES_DEST"], filename)
            tika_helper.init()
            text = tika_helper.parse(full_path)

        else:
            text = form.text.data

        text = prepare_text(text)

        sources = config_helper.app_config["sources"]

        docs = seta_helper.get_similar_docs_by_content(text, 10, sources)

        metadata = metadata_helper.extract_from_docs(
            docs["documents"], supported_properties
        )

        # for each key in meta, sort by score value
        # use sparql to get labels for each key

        for key in metadata:
            metadata[key] = sorted(metadata[key], key=lambda tup: tup[1], reverse=True)

        # TODO: get labels from vocabularies files to improve response time
        #  from metadata extract all concepts
        eurovoc_concepts = []
        for key in metadata:
            eurovoc_concepts.extend([tup[0] for tup in metadata[key]])

        sparql = sparql_op_helper.init_sparql_endpoint()

        eurovoc_labels = (
            sparql_op_helper.get_metadata_labels_from_cellar_sparql_endpoint2(
                eurovoc_concepts, sparql
            )
        )

        for key in metadata:
            metadata[key] = [
                (
                    eurovoc_labels[tup[0]],
                    tup[0],
                    tup[1],
                    # extract id from url in tup[0]
                )
                for tup in metadata[key]
            ]
        # add id to each tuple
        for key in metadata:
            metadata[key] = [
                (
                    tup[1].split("/")[-1],
                    formated_id(tup[1].split("/")[-1], key),
                    tup[0],
                    tup[1],
                    tup[2],
                )
                for tup in metadata[key]
            ]

        return render_template(
            "export.html",
            metadata=metadata,
            text=text,
            similar_docs=docs["documents"],
            is_file=is_file,
            form=form,
        )

    return render_template("upload_doc.html", form=form)


def prepare_text(text):
    text = clean.sentenced(text)
    limit_text_length = config_helper.app_config["limit_text_length"]
    if len(text) > limit_text_length:
        text = text[:limit_text_length]
    return text


def formated_id(id, property):
    #  if id contains letters return "$a id. $2 EUROVOC"
    # else return fixed format of 6 chars, replace remaining with 0, with  "$a 00 id. $2"
    if property == "cdm:work_is_about_concept_eurovoc":

        if any(char.isalpha() for char in id):
            return "$a " + id + ". $2 EUROVOC"
        else:
            return "$a " + id.zfill(6) + ". $2 EUROVOC"
    elif property == "cdm:publication_general_is_about_concept_op_theme":
        return "$a " + id + " $2 LU-LuOPE"
    else:
        return "$a " + id + ". $2"

    return render_template("upload_doc.html", form=form)


def reaload_seta():

    seta_helper.reload()

    web_app.logger.debug("seta_helper reloaded at " + str(datetime.datetime.now()))


eurovoc_labels = []


def load_eurovoc_labels():

    global eurovoc_labels
    df = pd.read_excel("/home/cellar_seta/web/uploads/eurovoc_export_en.xlsx", header=0)
    # convert df to array
    eurovoc_labels = df.values
    # convert eurovoc labels to array of tuples
    eurovoc_labels = [tuple(row) for row in eurovoc_labels]


# ajax response to search Eurovoc concepts with get request
# params : query
# return : json response
@web_app.route("/eurovoc_autocomplete", methods=["GET"])
def search_eurovoc():

    query = request.args.get("query")

    # search eurovoc labels which is array of tuples
    #  default dic of objects

    results = []
    key = 0
    for label in eurovoc_labels:
        if query.lower() in label[1].lower():
            results.append({"id": key, "text": label[1], "eurovoc_id": label[0]})
            key += 1

    return jsonify(results)


@web_app.route("/save_metadata_feedback", methods=["GET", "POST"])
def save_metadata():
    if request.method == "POST":

        feedback = json.loads(request.data)

        document_id = feedback["document_id"]
        similar_docs_ids = feedback["similar_docs_ids"]
        document_text = feedback["document_text"]
        rating = feedback["rating"]
        is_file = feedback["is_file"]
        comment = feedback["comment"]
        # foreach row in data, extract properties and values
        data = feedback["data"]
        # TODO : use supported properties
        properties = [
            "cdm:work_is_about_concept_eurovoc",
            "cdm:publication_general_is_about_concept_op_theme",
        ]

        # default dic of properties and values
        feedback_dic = defaultdict(list)
        feedback_dic["document_id"] = document_id
        feedback_dic["similar_docs_ids"] = similar_docs_ids
        feedback_dic["document_text"] = document_text
        feedback_dic["rating"] = rating
        feedback_dic["is_file"] = is_file
        feedback_dic["comment"] = comment

        for i in range(len(data)):

            # if id in properties, create dic and put following rows in it UNTIL next property
            if data[i]["id"] in properties:
                # create dic
                # put following rows in dic
                for j in range(i + 1, len(data)):
                    if data[j]["id"] in properties:
                        break
                    else:
                        feedback_dic[data[i]["id"]].append(data[j])

        saved = True

        feedback_logger = logging.getLogger("feedback_logger")
        feedback_logger.info(feedback_dic)

        message = "Metadata saved successfully. Thank you for your feedback."

        return jsonify({"saved": saved, "message": message})


def get_eurovoc_labels(concepts):

    sparql = sparql_op_helper.init_sparql_endpoint()

    labels = sparql_op_helper.get_metadata_labels_from_cellar_sparql_endpoint2(
        concepts, sparql
    )
    return labels


# querying rdf file
@web_app.route("/rdf", methods=["GET", "POST"])
def query_rdf():
    # get broader concepts of a concept
    sparql = sparql_op_helper.init_sparql_endpoint()
    # get concepts from request
    concepts = json.loads(request.data)

    # for each concept in  concepts, get broader concepts
    broader_concepts = defaultdict(list)
    all_broader_concepts = []
    for concept in concepts:
        B = sparql_op_helper.get_broader_concepts(concept, sparql)
        broader_concepts[concept] = B
        all_broader_concepts.extend(B)

    # for each concept in all broader concepts, get broader concepts
    for concept in all_broader_concepts:
        B = sparql_op_helper.get_broader_concepts(concept, sparql)
        broader_concepts[concept] = B
        all_broader_concepts.extend(B)

    # based on the broader concepts, reconstruct the tree using anytree
    # get labels of all broader concepts
    all_concepts = concepts + all_broader_concepts

    labels = get_eurovoc_labels(all_concepts)
    # convert concepts to nodes
    nodes = defaultdict(list)
    for concept in concepts:
        concept_id = concept.split("/")[-1]
        nodes[concept] = Node(concept_id, label=labels[concept])
        for broader_concept in broader_concepts[concept]:
            broader_concept_id = broader_concept.split("/")[-1]
            nodes[broader_concept] = Node(
                broader_concept_id, label=labels[broader_concept]
            )

    root = Node("root")
    # add broader concepts to concepts
    # TODO: check if this works always!!
    for concept in all_concepts:

        broader_concepts_of_concept = broader_concepts[concept]
        concept_node = nodes[concept]
        concept_node.parent = root

        if len(broader_concepts_of_concept) == 0:
            concept_node.parent = root

        else:
            for broader_concept in broader_concepts_of_concept:

                broader_concept_node = nodes[broader_concept]
                if broader_concept in all_concepts:
                    broader_concept_node.parent = concept_node.parent
                    concept_node.parent = broader_concept_node
                    break
                else:
                    broader_concept_node.parent = root

    for pre, fill, node in RenderTree(root):
        print("%s%s" % (pre, node.name))
    # export json

    return JsonExporter(indent=2, sort_keys=True).export(root)


# action : /get_feedback_file : returns feedback file to download
@web_app.route("/download_feedback_file", methods=["GET"])
def download_feedback_file():
    try:
        return send_file(
            "/home/cellar_seta/" + config_helper.app_config["feedback_log_file"]
        )
    except Exception as e:
        return str(e)


# download a statistics version of the feedback file
@web_app.route("/download_stats", methods=["GET"])
def download_feedback_file_statistics():
    # open feedback file
    feedback_file = open(
        "/home/cellar_seta/" + config_helper.app_config["feedback_log_file"], "r"
    )
    # for each entry, we have this format : date - feedback_dic
    # extract number of elments having selected : true
    number_of_selected_eurovoc_true = 0
    number_of_selected_eurovoc_false = 0
    number_of_selected_theme_true = 0
    number_of_selected_theme_false = 0
    number_of_feedbacks = 0
    csv_string = "date,document_id,eurovoc_selected_true,eurovoc_selected_false,eurovoc_is_manually_added,theme_selected_true,theme_selected_false,theme_is_manually_added \n"
    for line in feedback_file:
        print(line)
        number_of_feedbacks += 1
        document_id = line.split("document_id")[1].split(",")[0][4:-1]
        date = line[:19]
        # split line by cdm:work_is_about_concept_eurovoc
        two_properties = line.split("cdm:publication_general_is_about_concept_op_theme")
        eurovoc = two_properties[0]
        # count number of selected : true
        eurovoc_selected_true = eurovoc.count("'selected': True")
        eurovoc_selected_false = eurovoc.count("'selected': False")
        eurovoc_is_manually_added = eurovoc.count("'isManualyAdded': True")

        theme_selected_true = 0
        theme_selected_false = 0
        theme_is_manually_added = 0
        if len(two_properties) > 1:
            theme = two_properties[1]
            theme_selected_true = theme.count("'selected': True")
            theme_selected_false = theme.count("'selected': False")
            theme_is_manually_added = theme.count("'isManualyAdded': True")

        csv_string += (
            str(date)
            + ","
            + str(document_id)
            + ","
            + str(eurovoc_selected_true)
            + ","
            + str(eurovoc_selected_false)
            + ","
            + str(eurovoc_is_manually_added)
            + ","
            + str(theme_selected_true)
            + ","
            + str(theme_selected_false)
            + ","
            + str(theme_is_manually_added)
            + "\n"
        )
    # return csv file

    # generate csv file
    file_path = "/home/cellar_seta/web/logs/feedback_statistics.csv"
    csv_file = open(file_path, "w")
    csv_file.write(csv_string)
    csv_file.close()
    try:
        return send_file(file_path, mimetype="text/csv")
    except Exception as e:
        return str(e)


def init_list_of_supported_properties():
    global supported_properties
    properties = config_helper.app_config["supported_properties"]
    supported_properties = {}

    for property in properties:
        supported_properties[property] = defaultdict(list)
    return supported_properties


def init_feedback_logger():
    # global feedback_logger
    feedback_logger = logging.getLogger("feedback_logger")
    feedback_logger.setLevel(logging.DEBUG)

    log_file = config_helper.app_config["feedback_log_file"]
    feedback_logger_handler = logging.FileHandler(log_file)
    feedback_logger_handler.setLevel(logging.INFO)
    feedback_logger_formatter = logging.Formatter("%(asctime)s - %(message)s")

    feedback_logger_handler.setFormatter(feedback_logger_formatter)
    feedback_logger.addHandler(feedback_logger_handler)


# action test
@web_app.route("/test", methods=["GET"])
def test():
    # return a whole html doc
    return render_template("eurovoc_autocomplete.html")


# if __name__ == ("__main__"):

web_app.jinja_env.auto_reload = True
web_app.config["TEMPLATES_AUTO_RELOAD"] = True

# at start up delete seta token
try:
    os.remove("seta.token")
    web_app.logger.debug("Deleted seta token")
except:
    web_app.logger.debug("No token file")

scheduler = BackgroundScheduler()

scheduler.add_job(func=reaload_seta, trigger="interval", minutes=50, timezone="UTC")
scheduler.start()
import atexit

# Shut down the scheduler when exiting the app
atexit.register(lambda: scheduler.shutdown())

# load eurovoc labels
load_eurovoc_labels()

init_list_of_supported_properties()

init_feedback_logger()

web_app.run(host="0.0.0.0", debug=True)
