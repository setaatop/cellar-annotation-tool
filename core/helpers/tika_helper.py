import tika

from tika import parser
from core.utils import clean

from core.helpers import (
    config_helper,
)


def init():
    tika.initVM()


def parse(file=""):

    parsed = parser.from_file(file)
    return parsed["content"]


def prepare_text(text):
    text = clean.sentenced(text)
    limit_text_length = config_helper.app_config["limit_text_length"]
    if len(text) > limit_text_length:
        text = text[:limit_text_length]
    return text
