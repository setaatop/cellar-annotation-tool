from collections import defaultdict
from typing import DefaultDict
import yaml
from SPARQLWrapper import Wrapper, SPARQLWrapper, SPARQLWrapper2
import urllib
from jinja2 import Template

# query cellar sparql endpoint to get broader concepts of eurovoc concept


def get_broader_concepts(concept, sparql=None):

    query = prepare_broader_concepts_query(concept)

    sparql.setQuery(query)

    broader_concepts = []

    for result in sparql.query().bindings:
        broader_concepts.append(result["broader"].value)

    return broader_concepts


def prepare_broader_concepts_query(concept):

    query_template = """
            
            {{prefixes}}
            
            select ?broader where {
    
            <{{concept}}> skos:broader/skos:broader* ?broader.
    
            }
    
            """
    rendered_query = Template(query_template).render(
        concept=concept,
        prefixes=app_config["prefixes"],
    )
    return rendered_query


def init_sparql_endpoint():
    sparql = SPARQLWrapper2(app_config["cellar_sparql_endpoint"])
    sparql.setMethod("POST")
    return sparql


def prepare_metadata_dic(metadata_properties):
    """
    replace "-" by "_" in properties to be able to use them in query variables
    """
    metadata_properties_dic = {}
    for metadata_property in metadata_properties:
        variable = metadata_property.split(":")[1].replace("-", "_")
        metadata_properties_dic[variable] = metadata_property
    return metadata_properties_dic


def get_metadata_from_cellar_sparql_endpoint(
    metadata_properties=[], sparql=None, doc_id=None
):

    metadata_properties_dic = prepare_metadata_dic(metadata_properties)

    query = prepare_query(metadata_properties_dic, doc_id)
    sparql.setQuery(query)

    document_metadata = defaultdict()
    # TODO: check documents that have no metadata
    for result in sparql.query().bindings:
        for variable, property in metadata_properties_dic.items():
            medata_list = result[variable].value.split(",")
            # store results in elasticsearch
            # es store_metadata_in_elasticsearch()
            if len(medata_list) > 0 and medata_list[0] != "":
                document_metadata[property] = medata_list

    return document_metadata


def prepare_query(metadata_properties_dic, work_id):

    # TODO: encode all work_id ?

    work_id = encode_url(work_id)

    query_template = """
        
        {{prefixes}}
        
        select distinct
          
          {% for variable, property in metadata_properties_dic.items() %}

          (group_concat(distinct ?{{variable}};separator=",") as ?{{variable}})
          
          {%- endfor %}
          
          (group_concat(distinct ?doc_id;separator=",") as ?ids)
          
        where {
          
          {% if "cellar:" not in work_id %}
           
           ?cellar_id owl:sameAs {{work_id}}.
           ?cellar_id cdm:work_id_document ?doc_id.
          
           
           {% for variable, property in metadata_properties_dic.items() %}
            OPTIONAL{
            ?cellar_id {{property}} ?{{variable}}.
            }
          {%- endfor %}
          
          {% else %}
          {{work_id}} cdm:work_id_document ?doc_id.
          
          {% for variable, property in metadata_properties_dic.items() %}
          OPTIONAL{
            {{work_id}} {{property}} ?{{variable}}.
            }
          {%- endfor %}
          
          {% endif %}
          
        }

        """
    rendered_query = Template(query_template).render(
        metadata_properties_dic=metadata_properties_dic,
        work_id=work_id,
        prefixes=app_config["prefixes"],
    )

    return rendered_query


def prepare_metadata_label_query(metadata):

    query_template = """
        
        {{prefixes}}
        
        select ?label where {

        <{{metadata}}> <http://www.w3.org/2004/02/skos/core#prefLabel>  ?label.
            FILTER (lang(?label) = 'en')
        }

        """
    # if metadata contains cdm# , prepare new query
    if "cdm#" in metadata:

        query_template = """
        
        {{prefixes}}
        
        select ?label where {

        <{{metadata}}> <http://www.w3.org/2000/01/rdf-schema#label>  ?label.
           
        }

        """

    rendered_query = Template(query_template).render(
        metadata=metadata,
        prefixes=app_config["prefixes"],
    )

    return rendered_query


def prepare_get_labels_query(concepts):
    # Define the SPARQL template
    query_template = """
        
        {{prefixes}}
        
       SELECT ?concept ?label WHERE {
    VALUES ?concept {
      {% for concept in concepts %}
      <{{ concept }}>
      {% endfor %}
    }
    ?concept a skos:Concept ;
            skos:prefLabel ?label .
    FILTER (lang(?label) = 'en')
  }

        """

    # Render the SPARQL query using the template and the list of concepts
    rendered_query = Template(query_template).render(
        concepts=concepts,
        prefixes=app_config["prefixes"],
    )

    # Return the resulting SPARQL query
    return rendered_query


# TODO : use local eurovoc vocabulary file
def get_metadata_labels_from_cellar_sparql_endpoint(meta, sparql=None):

    query = prepare_metadata_label_query(meta)

    sparql.setQuery(query)

    document_metadata = defaultdict()

    # return label
    label = meta
    bind = sparql.query().bindings
    if len(bind) > 0:
        label = bind[0]["label"].value
    # if label empty return default value

    return label


def get_metadata_labels_from_cellar_sparql_endpoint2(concepts, sparql=None):

    query = prepare_get_labels_query(concepts)

    sparql.setQuery(query)

    document_metadata = defaultdict()
    # extract concepts and labels
    bind = sparql.query().bindings
    for result in bind:
        concept = result["concept"].value
        label = result["label"].value
        document_metadata[concept] = label

    return document_metadata


def encode_url(work_id):
    work_id_splited = work_id.split(":")
    prefix = work_id_splited[0]
    id = work_id_splited[1]
    id = urllib.parse.quote_plus(id)
    work_id = prefix + ":" + id
    return work_id


config = yaml.load(open("/home/cellar_seta/config/config.yaml"), Loader=yaml.FullLoader)
app_config = yaml.load(
    open("/home/cellar_seta/config/app/config.yaml"), Loader=yaml.FullLoader
)
