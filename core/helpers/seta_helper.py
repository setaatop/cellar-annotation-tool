from collections import defaultdict
import gzip
import json
import logging
import os
import sys
import requests

import yaml
import urllib
from core.utils import clean

from core.helpers import config_helper, sparql_op_helper

sys.path.append(os.path.join(os.path.dirname(__file__), "../.."))

import core.utils.seta_api_utils as seta_api_utils
from core.helpers import tika_helper


token_json, api_url = seta_api_utils.init_seta_api()

# function reload to update token
def reload():
    """Reload the seta config to renew token.
    Then, perform a query to wakeup seta models

    # Returns



    """
    global token_json, api_url
    token_json, api_url = seta_api_utils.init_seta_api(force=True)

    logging.debug("init SeTA...")
    try:
        get_similar_docs_by_content("Energy")
    except:
        logging.debug("Done ")


def headers():
    global token_json, api_url
    token_json, api_url = seta_api_utils.init_seta_api()
    headers_ = {"Authorization": token_json["access_token"]}
    return headers_


app_config = yaml.load(
    open("/home/cellar_seta/config/app/config.yaml"), Loader=yaml.FullLoader
)


def get_similar_docs(doc_id, es, n_docs, source):

    index = "similar_docs" + "_".join(source)

    logging.Logger.debug("getting sim docs of", doc_id)

    payload = {
        "source": source,
        "semantic_sort_id": doc_id,
        "n_docs": n_docs,
    }

    r = requests.get(api_url + "corpus", params=payload, headers=headers()())
    r_json = r.json()

    similar_documents = r_json["documents"]
    similar_documents_ids = []
    similar_documents_ids_dict = defaultdict()

    for similar_doc_id in similar_documents:
        similar_documents_ids.append(similar_doc_id["id"])
    similar_documents_ids_dict["similar_documents"] = similar_documents_ids

    return similar_documents_ids


def get_doc(doc_id):

    doc_id = urllib.parse.quote_plus(doc_id)
    r = requests.get(api_url + "corpus/" + doc_id, headers=headers())
    r_json = json.loads(r.text)
    return r_json


def similarity_score_(doc_id_1, doc_id_2, round_to_decimals=2):
    doc_1 = get_doc(doc_id_1)
    doc_2 = get_doc(doc_id_2)
    emb_1 = doc_1["sbert_vector"]
    emb_2 = doc_2["sbert_vector"]
    # cosine sim
    score = util.cos_sim(emb_1, emb_2)
    return round(score.numpy()[0][0], round_to_decimals)


def similarity_score(doc_1, doc_2, round_to_decimals=2):

    emb_1 = doc_1["sbert_vector"]
    emb_2 = doc_2["sbert_vector"]
    # cosine sim
    score = util.cos_sim(emb_1, emb_2)
    return round(score.numpy()[0][0], round_to_decimals)


def similar_docs(doc_id, n_docs=11, source=[]):
    payload = {
        "source": source,
        "semantic_sort_id": doc_id,
        "n_docs": n_docs,
    }
    # perform your request(in this case corpus api)
    r = requests.get(api_url + "corpus", params=payload, headers=headers())
    r_json = r.json()
    similar_documents = r_json["documents"]
    return similar_documents


def get_similar_docs_by_content(text="", n_docs=10, sources=[]):
    emb_vector = compute_embeddings_vector(text)
    r_json = get_similar_docs_by_vector(emb_vector, n_docs, sources)
    return r_json


def get_similar_docs_by_vector(emb_vector, n_docs, sources=[]):
    data = {
        "emb_vector": emb_vector,
        "n_docs": n_docs,
        "source": sources,
        "date_range": ["lte:2100-01-01"],  # TODO : to be removed when seta is updated
        # this filter was added to avoid getting eurovoc concept that have been
        # added to the corpus in unused dates to perform matching analysis
    }
    logging.debug("getting similar docs using vector")
    r = requests.post(api_url + "corpus", data=json.dumps(data), headers=headers())
    if r.status_code == 200:
        r_json = r.json()
        return r_json
    raise Exception("SeTA error getting similar docs", r.status_code, r.text)


def compute_embeddings_vector(text, cleaning_reduction=True):

    if cleaning_reduction:
        text = clean.sentenced(text)
        if len(text) > 5000:
            text = text[:5000]

    payload = {
        "text": text,
    }

    logging.debug("getting vector")
    try:
        r = requests.post(
            api_url + "compute_embeddings", params=payload, headers=headers()
        )
        r_json = r.json()
        return r_json["embeddings"]["vector"]
    except Exception as e:
        logging.debug("error getting vector", e)
        raise e


# def compute_embeddings_vector(text):

#     payload = {
#         "text": text,
#     }
#     logging.debug("getting vector")
#     try:
#         r = requests.post(
#             api_url + "compute_embeddings", params=payload, headers=headers()
#         )
#         r_json = r.json()
#         return r_json["embeddings"]["vector"]
#     except Exception as e:
#         logging.debug("error getting vector", e)
#         raise e


def init_token():
    global token_json
    global headers
    token_json, api_url = seta_api_utils.init_seta_api()
    headers = {"Authorization": token_json["access_token"]}
