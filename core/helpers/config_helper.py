import yaml


seta_config = yaml.load(
    open("/home/cellar_seta/config/config.yaml"), Loader=yaml.FullLoader
)
app_config = yaml.load(
    open("/home/cellar_seta/config/app/config.yaml"), Loader=yaml.FullLoader
)
