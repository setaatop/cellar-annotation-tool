from collections import defaultdict
import os
import sys


# TODO : check why this is used
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

# app_config = yaml.load(open("config/app/config.yaml"), Loader=yaml.FullLoader)


from core.utils import seta_api_utils
from core.helpers import sparql_op_helper


token_json, api_url = seta_api_utils.init_seta_api()

# put guest token in Authorization header
headers = {"Authorization": token_json["access_token"]}


from helpers import seta_helper


def extract_from_docs(
    documents,
    properties={},
    threshold=0,
):

    sparql = sparql_op_helper.init_sparql_endpoint()

    similar_documents_with_cellar_metadata = []

    for doc in documents:

        similar_doc_id = doc["id"]

        similar_doc_metadata = get_doc_metadata_from_cellar(
            similar_doc_id, sparql, properties
        )

        similar_doc_metadata["id"] = similar_doc_id
        similar_documents_with_cellar_metadata.append(similar_doc_metadata)

    metadata = extract_from_similar_docs(
        similar_documents_with_cellar_metadata, properties, threshold
    )

    for key in metadata:
        metadata[key] = sorted(metadata[key], key=lambda tup: tup[1], reverse=True)

    return metadata


def extract_from_similar_docs(similar_documents, properties, threshold=0):

    similar_docs_metadata = extract_metadata_foreach_doc(similar_documents, properties)

    metadata_occurence = count_metadata_occurence(
        similar_documents, properties, similar_docs_metadata
    )

    filtered_metadata_list = filter_metadata_by_occurence(threshold, metadata_occurence)

    return filtered_metadata_list


def filter_metadata_by_occurence(threshold, metadata_occurence):

    filtered_metadata_list = defaultdict(list)

    for property, occurence_dict in metadata_occurence.items():
        for similar_doc_metadata_value, occurence in occurence_dict.items():
            # round to 2 decimals
            occurence = round(occurence, 2)
            if occurence >= threshold:
                filtered_metadata_list[property].append(
                    (similar_doc_metadata_value, occurence)
                )
    return filtered_metadata_list


def count_metadata_occurence(similar_documents, properties, all_similar_docs_metadata):
    # returns [property][metadata] = [occurence]
    metadata_values_occurence = defaultdict(dict)
    total = len(all_similar_docs_metadata)

    # TODO : check diff between using similar_documents vs all_similar_docs_metadata
    # roughly not diff
    # total = len(similar_documents)
    # >> some cases all_similar_docs_metadata < similar_documents
    # because the property is empty TOCHECH

    for id_sim_doc, all_sim_doc_metadata in all_similar_docs_metadata.items():
        for property, _ in properties.items():
            if property in all_sim_doc_metadata:
                doc_metadata_values = all_sim_doc_metadata[property]
                for similar_doc_metadata_value in doc_metadata_values:
                    if (
                        similar_doc_metadata_value
                        in metadata_values_occurence[property]
                    ):
                        metadata_values_occurence[property][
                            similar_doc_metadata_value
                        ] += (1 / total)

                    else:
                        metadata_values_occurence[property][
                            similar_doc_metadata_value
                        ] = (1 / total)
    return metadata_values_occurence


def extract_metadata_foreach_doc(similar_documents, properties):
    #  returns [doc_id][property] = [metadata]
    all_similar_docs_metadata = defaultdict(dict)

    # get all
    similar_doc_ids = []
    for property, _ in properties.items():

        for similar_doc in similar_documents:
            similar_doc_id = similar_doc["id"]
            similar_doc_ids.append(similar_doc_id)

            if (
                property in similar_doc
                and similar_doc[property] is not None
                and len(similar_doc[property]) > 0
                and similar_doc[property][0] != ""
            ):

                similar_doc_metadata = similar_doc[property]

                all_similar_docs_metadata[similar_doc["id"]][
                    property
                ] = similar_doc_metadata

    return all_similar_docs_metadata


# TODO : move to sparql helper
def get_doc_metadata_from_cellar(doc_id, sparql, properties):

    similar_doc_metadata = sparql_op_helper.get_metadata_from_cellar_sparql_endpoint(
        metadata_properties=properties,
        sparql=sparql,
        doc_id=doc_id,
    )

    similar_doc_metadata["id"] = doc_id

    return similar_doc_metadata
