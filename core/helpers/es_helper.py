import time
from elasticsearch import Elasticsearch
import yaml


# using methods below create new class called es_helper

IDENTIFIERS_INDEX = "identifiers"
METADATA_INDEX = "metadata"
RANDOM_SIM_DOCS_INDEX = "random_sim_docs"
CLASS_DOCS_INDEX = "class_docs_2"
PRIORITIES_DOCS_INDEX = "eu_priorities_docs"
SETA_TERMS_INDEX = "seta_terms"
METADATA_FEEDBACK_INDEX = "metadata_feedback"


# TODO : use config_helper instead
config = yaml.load(open("/home/cellar_seta/config/config.yaml"), Loader=yaml.FullLoader)
app_config = yaml.load(
    open("/home/cellar_seta/config/app/config.yaml"), Loader=yaml.FullLoader
)
es = Elasticsearch(config["es-host"])

SETA_DOCS_INDEX = config["index"]



# def doc_exists_in_es(es, doc_id):
#     return es.exists(index="metadata", id=doc_id)


def doc_exists(es, index, doc_id):
    return es.exists(index=index, id=doc_id)


def similar_doc_exists_in_es(es, doc_id, index):
    return es.exists(index=index, id=doc_id)


def store_similar_docs_in_es(es, doc_id, similar_documents, index):
    res = es.index(index=index, id=doc_id, document=similar_documents)
    return res


def store_doc(es, index, id, doc):
    res = es.index(index=index, id=id, document=doc)
    return res


def count_docs(es, index):
    total = es.count(index=index)["count"]
    return total


# TODO :refactor define one function to get all docs
def store_metadata_in_es(es, doc_id, documents_metadata):
    res = es.index(index="metadata", id=doc_id, body=documents_metadata)
    return res


def get_doc(es, index, doc_id):
    if es.exists(index=index, id=doc_id):
        res = es.get(index=index, id=doc_id)
        return res["_source"]
    return None


def get_metadata_from_es(es, doc_id):
    if es.exists(index="metadata", id=doc_id):
        res = es.get(index="metadata", id=doc_id)
        return res["_source"]
    return None


def init_es():
    # if es is None:
    #     es = Elasticsearch(config["es-host"])
    return es


def get_random_docs(es, index, size=10, filter=None):

    if filter is None:
        filter = [
            {
                "bool": {
                    "should": [
                        {"prefix": {"id": "celex:"}},
                        {"prefix": {"id": "cellar:"}},
                    ]
                }
            }
        ]

    # TODO : use scan to get more than 10k docs

    res = es.search(
        index=index,
        size=size,
        query={
            "function_score": {
                "query": {
                    "bool": {
                        "must": {"match_all": {}},
                        "filter": filter,
                    }
                },
                "random_score": {},
            }
        },
    )
    return res
