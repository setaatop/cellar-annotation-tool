from collections import defaultdict
from genericpath import isfile
from ntpath import join
import os
import numpy as np

from core.helpers import tika_helper, seta_helper, es_helper

from sentence_transformers import util


class classifier:
    """
    baseline_docs: policy -> doc content
    """

    def __init__(self, seta_helper, baseline_docs):
        self.seta_helper = seta_helper
        self.baseline_docs = baseline_docs
        self.baseline_docs_emb_vector = defaultdict()
        self.compute_baseline_docs_embeddings()

    def compute_baseline_docs_embeddings(self):

        for topic, baseline_doc in self.baseline_docs.items():
            self.baseline_docs_emb_vector[topic] = self.get_doc_emb_vector(baseline_doc)

    def classify(self, doc):
        # compute doc vector
        # for each doc in baseline docs
        # compute similarity
        # return doc with highest similarity
        result = dict()
        # if doc contains sbert embeddings
        doc_vector = self.get_doc_emb_vector(doc)

        for topic, topic_doc_embedding in self.baseline_docs_emb_vector.items():
            # TODO use different library for similarity, utili is from sbert
            similarity = util.cos_sim(doc_vector, topic_doc_embedding)
            # get tensor value

            result[topic] = similarity.numpy()[0][0]

        #  sort by similarity
        result = dict(sorted(result.items(), key=lambda tup: tup[1], reverse=True))
        return result
        # return sorted(result.items(), key=lambda x: x[1], reverse=True)
        # return result

    def get_doc_emb_vector(self, doc):
        if "sbert_vector" in doc:
            doc_vector = doc["sbert_vector"]
        else:
            doc_vector = self.seta_helper.compute_embeddings_vector(doc)
        return doc_vector

    @staticmethod
    def get_base_docs(docs_directory):

        docs_ids = []

        for filename in os.listdir(docs_directory):
            if isfile(join(docs_directory, filename)):
                docs_ids.append(filename.split(".")[0])
            # docs_ids.append(filename.split(".")[0])

        es = es_helper.init_es()

        docs = defaultdict()

        for doc_id in docs_ids:
            doc = es_helper.get_doc(es, es_helper.CLASS_DOCS_INDEX, doc_id)
            docs[doc_id] = doc

        return docs

    @staticmethod
    def prepare_base_docs(docs_directory):
        docs_content = classifier.load_base_docs(docs_directory)
        es = es_helper.init_es()
        for key, content in docs_content.items():
            # Check if the document already exists
            if not es_helper.doc_exists(es, es_helper.CLASS_DOCS_INDEX, key):
                # replace _ with space
                title = key.replace("_", " ")

                sbert_vector = seta_helper.compute_embeddings_vector(content)
                doc = {
                    "id": key,
                    "title": title,
                    "content": content,
                    "sbert_vector": sbert_vector,
                }
                es_helper.store_doc(es, es_helper.CLASS_DOCS_INDEX, key, doc)

    @staticmethod
    def load_base_docs(docs_directory):
        tika_helper.init()
        docs_content = defaultdict()
        #  if file name is a file
        for filename in os.listdir(docs_directory):
            if isfile(join(docs_directory, filename)):
                file = docs_directory + filename
                content = tika_helper.parse(file)
                key = filename.split(".")[0]
                docs_content[key] = content
        return docs_content

    # @staticmethod
    # def store_base_docs(docs_content):

    #     es = es_helper.init_es()
    #     for key, content in docs_content.items():
    #         # replace _ with space
    #         title = key.replace("_", " ")
    #         sbert_vector = seta_helper.compute_embeddings_vector(content)
    #         doc = {
    #             "id": key,
    #             "title": title,
    #             "content": content,
    #             "sbert_vector": sbert_vector,
    #         }
    #         es_helper.store_doc(es, es_helper.CLASS_DOCS_INDEX, key, doc)
