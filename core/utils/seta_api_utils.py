import logging
import yaml
import requests
import json
import os

# TODO use config file
config = yaml.load(open("/home/cellar_seta/config/config.yaml"), Loader=yaml.FullLoader)

api_url = (
    "http://" + config["api-host"] + ":" + str(config["api-port"]) + "/seta-api/api/v1/"
)

# TODO: to move into seta_helper


def init_seta_api(force = False):
    # if token exists, check if it is still valid
    # if not, get a new one
    if os.path.exists("seta.token") and not force:
        logging.debug("using old TOEKN!")
        with open("seta.token", "r") as f:
           guest_token = json.load(f)
           return guest_token, api_url
    else:
        guest_token = requests.get(api_url + "get-token")
        token_json = json.loads(guest_token.text)
        # save token
        logging.debug("GETTING new TOEKN!")
        
        with open("seta.token", "w") as f:
            json.dump(token_json, f)
        return token_json, api_url
