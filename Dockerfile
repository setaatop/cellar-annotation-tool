# base image contains the dependencies and no application code
FROM ubuntu:20.04 as base

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y
RUN apt-get install -y python3-pip python-dev build-essential

# TODO :  remove java after fixing tika issue in SeTA | use lighter image like alpine
USER root

# Install OpenJDK-11
RUN apt update && \
    apt-get install -y openjdk-11-jdk && \
    apt-get install -y ant && \
    apt-get clean;

# Set JAVA_HOME
ENV JAVA_HOME /usr/lib/jvm/java-11-openjdk-amd64/
RUN export JAVA_HOME

RUN useradd -m cellar_seta_user 

ARG ROOT=/home/cellar_seta
WORKDIR $ROOT

RUN chown -R cellar_seta_user /home/cellar_seta 

COPY requirements.txt $ROOT/requirements.txt
RUN pip install -r $ROOT/requirements.txt

FROM base as debug

RUN pip install debugpy
# put FLASK_ENV=development
ENV FLASK_ENV=development
ENV FLASK_APP=web/web_app.py
CMD python3 -m debugpy --listen 0.0.0.0:5678 --wait-for-client -m flask run -h 0.0.0 -p 5000

# dev image inherits from base and adds application code
FROM base as dev
# ADD . .
ENV FLASK_ENV=development
# CMD ["python3", "/home/cellar_seta/web/web_app.py"] # todo use gunicorn
# add also gunicorn --bind "0.0.0.0:8089" api:app

# prod image inherits from base and adds application code
FROM base as prod
ADD . .
CMD ["python3", "/home/cellar_seta/web/web_app.py"]