import unittest
import sys, os


sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

from core.metadata_enrichment import *
from core.helpers.seta_helper import *

from core.metadata_enrichment_stats import similar_docs2


# TODO: prepare test dataset, undependent of datasource.


class TestGetDoc(unittest.TestCase):
    """
    Our basic test class
    """

    def test_get_doc(self):
        existing_doc_id = "CELEX:52013SC0076"
        res = get_doc(existing_doc_id)
        self.assertEqual(res["longid"], existing_doc_id)

    def test_get_similar_docs_by_content(self):
        text = "education"
        res = get_similar_docs_by_content(text)
        self.assertIn("documents", json.dumps(res))

    def test_get_similar_docs_of_doc(self):
        existing_doc_id = "CELEX:52013SC0076"
        res = similar_docs(existing_doc_id)
        self.assertIn("id", json.dumps(res))
       

if __name__ == "__main__":
    unittest.main()
