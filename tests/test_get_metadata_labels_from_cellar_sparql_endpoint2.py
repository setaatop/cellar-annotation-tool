import unittest
from get_metadata_labels_from_cellar_sparql_endpoint2 import get_metadata_labels_from_cellar_sparql_endpoint2

class TestGetMetadataLabelsFromCellarSparqlEndpoint2(unittest.TestCase):
    def test_get_metadata_labels_from_cellar_sparql_endpoint2(self):
        # Test with sample metadata and sparql query results
        meta = {"http://eurovoc.europa.eu/1234": "tax harmonisation", "http://eurovoc.europa.eu/5678": "plumbing equipment"}
        
        expected_output = {"concept1": "label1", "concept2": "label2"}
        self.assertEqual(get_metadata_labels_from_cellar_sparql_endpoint2(meta, sparql_results), expected_output)
        
        # Test with empty metadata
        meta = {}
        sparql_results = []
        expected_output = {}
        self.assertEqual(get_metadata_labels_from_cellar_sparql_endpoint2(meta, sparql_results), expected_output)
        
        # Test with metadata having a concept that is not in the sparql results
        meta = {"concept1": "label1", "concept2": "label2", "concept3": "label3"}
        sparql_results = [{"concept": "concept1", "label": "label1"}, 
                          {"concept": "concept2", "label": "label2"}]
        expected_output = {"concept1": "label1", "concept2": "label2"}
        self.assertEqual(get_metadata_labels_from_cellar_sparql_endpoint2(meta, sparql_results), expected_output)

if __name__ == '__main__':
    unittest.main()