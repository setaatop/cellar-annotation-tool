import json
import unittest
import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from core.helpers.seta_helper import get_doc
from core.utils.seta_api_utils import init_seta_api

class TestApiHealth(unittest.TestCase):
    """
    Our basic test class
    """

    def test_api_health(self):
        
        token_json = init_seta_api()
        self.assertIn('access_token', json.dumps(token_json))
       

if __name__ == '__main__':
    unittest.main()