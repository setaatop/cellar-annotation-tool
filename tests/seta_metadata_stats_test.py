import unittest
import sys, os

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

from app.metadata_enrichment import *
from core.helpers.seta_helper import get_doc, similar_docs



class TestGetDoc(unittest.TestCase):
    """
    Our basic test class
    """

    def test_doc_metadata_stats_common(self):
        doc_id = "CELEX:32003R0653"
        properties = {}
        properties["eurovoc"] = []
        properties["subject"] = []
        doc = get_doc(doc_id)
        similar_documents = similar_docs(doc_id)

        doc_metadata_stats_common(doc, similar_documents, properties)
    
    


if __name__ == "__main__":
    unittest.main()
