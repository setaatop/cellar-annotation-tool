try:
    from flask import Flask, request, jsonify, redirect, abort
    from flask_restful import Resource, Api
    from apispec import APISpec
    from apispec.ext.marshmallow import MarshmallowPlugin
    from flask_apispec.extension import FlaskApiSpec
    from flask_apispec.views import MethodResource
    from flask_apispec import doc



except Exception as e:
    print("Error: {} ".format(e))

import os, sys
from resources.classify import Classify


module_path = os.path.abspath(os.getcwd() + "/..")
if module_path not in sys.path:
    sys.path.append("/home/cellar_seta/")
######################## Helpers ########################


from resources.annotate import Annotate



######################## Flask app ########################
app = Flask(__name__)  # Flask app instance initiated
api = Api(app)  # Flask restful wraps Flask app around it.
print("Flask app instance initiated")
app.config.update(
    {
        "APISPEC_SPEC": APISpec(
            title="Cellar-SeTA Project",
            version="v1",
            plugins=[MarshmallowPlugin()],
            openapi_version="2.0.0",
        ),
        "APISPEC_SWAGGER_URL": "/swagger/",  # URI to access API Doc JSON
        "APISPEC_SWAGGER_UI_URL": "/swagger-ui/",  # URI to access UI of API Doc
    }
)
docs = FlaskApiSpec(app)


################## Handling errors ##################
# Return validation errors as JSON
@app.errorhandler(422)
@app.errorhandler(400)
def handle_error(err):
    headers = err.data.get("headers", None)
    messages = err.data.get("messages", ["Invalid request."])
    if headers:
        return jsonify({"errors": messages}), err.code, headers
    else:
        return jsonify({"errors": messages}), err.code


debug = True

######################## Routes ########################


class Health(MethodResource, Resource):
    @doc(description="This is health Endpoint", tags=["Health Endpoint"])
    def get(self):
        _ = {"message": "API is working fine"}
        return _


# @app.errorhandler(Exception)
# def handle_exception(e):
#     if isinstance(e, HTTPException):
#         return e

#     response = {
#         "code": 500,
#         "errorType": "Internal Server Error",
#         "errorMessage": "Something went really wrong!",
#     }
#     if debug:
#         response["errorMessage"] = f"{e}"

#     return jsonify(response), 500




# index endpoint to redirect to swagger UI
@app.route("/", methods=["GET"])
def index():
    return redirect("/swagger-ui/")


api.add_resource(Annotate, "/annotate")
api.add_resource(Classify, "/classify")
api.add_resource(Health, "/health")



if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5005)
