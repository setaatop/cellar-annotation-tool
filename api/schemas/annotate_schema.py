from collections import defaultdict
from marshmallow import ValidationError, fields, Schema, validates_schema
import os, sys

module_path = os.path.abspath(os.getcwd() + "/..")

if module_path not in sys.path:
    sys.path.append("/home/cellar_seta/")

from core.helpers import config_helper


class annotateSchema(Schema):
    """Annotate Schema"""

    # Class Attributes

    text = fields.Str(required=True)
    # file field

    # file = Field(metadata={"type": "string", "format": "byte"}, allow_none=True)

    properties = fields.List(fields.Str(), required=False)

    filter_value = fields.Float(required=False, default=0.0)

    # validate schema, if property is not in supported_properties, raise error
    @validates_schema
    def validate_properties(self, in_data, **kwargs):
        errors = {}
        supported_properties = config_helper.app_config["supported_properties"]
        properties = in_data.get("properties", None)
        if properties:
            for property in properties:
                if property not in supported_properties:
                    errors["properties"] = [
                        f"Invalid property: {property}. Supported properties are {supported_properties}"
                    ]
                    raise ValidationError(errors)
        return in_data

    # @validates_schema
    # def validate_uploaded_file(self, in_data, **kwargs):
    #     errors = {}
    #     file: FieldStorage = in_data.get("file", None)

    #     if file is None:
    #         # if any file is not uploaded, skip validation
    #         pass

    #     elif type(file) != FileStorage:
    #         errors["icon_url"] = [f"Invalid content. Only PNG, JPG/JPEG files accepted"]
    #         raise ValidationError(errors)

    #     elif file.content_type not in {
    #         "text/plain",
    #         "application/pdf",
    #         "application/docx",
    #         "application/doc",
    #     }:
    #         errors["icon_url"] = [
    #             f"Invalid file_type: {file.content_type}. Only pdf, docx, doc files accepted"
    #         ]
    #         raise ValidationError(errors)

    #     return in_data

    # @post_load
    # def post_load(self, loaded_obj, **kwargs):
    #     if loaded_obj.icon_url:
    #         sec_filename = secure_filename(
    #             f'{loaded_obj.name}.{loaded_obj.icon_url.filename.split(".")[-1]}'
    #         )
    #         loaded_obj.icon_url.save(
    #             f"{current_app.config['PUBLIC_IMAGES_FOLDER']}{sec_filename}"
    #         )
    #         loaded_obj.icon_url = (
    #             f'{current_app.config["PUBLIC_IMAGES_URL"]}{sec_filename}'
    #         )
    #     return loaded_obj
