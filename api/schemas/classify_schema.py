from collections import defaultdict
from marshmallow import ValidationError, fields, Schema, validates_schema
import os, sys

module_path = os.path.abspath(os.getcwd() + "/..")

if module_path not in sys.path:
    sys.path.append("/home/cellar_seta/")

from core.helpers import config_helper


class ClassifySchema(Schema):
    
   
    text = fields.Str(required=True)
   
 