

try:
    from flask import Flask, request, jsonify, redirect, abort
    from flask_restful import Resource, Api
    from apispec import APISpec
    from marshmallow import Schema, fields
    from apispec.ext.marshmallow import MarshmallowPlugin
    from flask_apispec.extension import FlaskApiSpec
    from flask_apispec.views import MethodResource
    from flask_apispec import marshal_with, doc, use_kwargs
    from schemas.classify_schema import ClassifySchema
    from werkzeug.exceptions import HTTPException
    from collections import defaultdict
    
    from core.document_classifier import classifier

    
    from core.helpers import (
    config_helper,
    seta_helper,
)

except Exception as e:
    print("Error: {} ".format(e))



class Classify(MethodResource, Resource):
    @doc(
        description="Classify a given document/text according to the EU priorities",
        tags=["Classification Endpoint"],
    )
    @use_kwargs(ClassifySchema)
    def post(self, **kwargs):

        text = kwargs["text"]

        data_dir = config_helper.app_config["data_dir"]
        base_docs_dir = data_dir + "Ten/"

        classifier.prepare_base_docs(base_docs_dir)

        base_docs = classifier.get_base_docs(base_docs_dir)
        classifier_ = classifier(seta_helper, base_docs)

        classes = classifier_.classify(text)
        
        classes_response = self.prepare_classes_response(classes)
       

        return classes_response, 200
    
    
    def prepare_classes_response(self,classes):
        
        for key, value in classes.items():
            classes[key] = float(value)
        policies = []
        for policy_key, score in classes.items():
            # Extract the policy ID and name from the policy key
            policy_id, policy_name = policy_key.split("_", 1)
            policy_name = policy_name.replace("_", " ")
            policies.append({"name": policy_name, "id": policy_id, "score": score})
        
        # Sort the policies by their score in descending order
        sorted_policies = sorted(policies, key=lambda p: p["score"], reverse=True)

        # Create a new dictionary with the sorted policies
        return {"policies": sorted_policies}