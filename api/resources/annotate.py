try:
    from flask import Flask, request, jsonify, redirect, abort
    from flask_restful import Resource, Api
    from apispec import APISpec
    from marshmallow import Schema, fields
    from apispec.ext.marshmallow import MarshmallowPlugin
    from flask_apispec.extension import FlaskApiSpec
    from flask_apispec.views import MethodResource
    from flask_apispec import marshal_with, doc, use_kwargs
    from schemas.annotate_schema import annotateSchema
    from werkzeug.exceptions import HTTPException
    from collections import defaultdict
    
    from core.helpers import (
    config_helper,
    seta_helper,
    metadata_helper,
    sparql_op_helper,
    tika_helper,
)


except Exception as e:
    print("Error: {} ".format(e))


class Annotate(MethodResource, Resource):
    @doc(
        description="Suggest annotation for a given document/text",
        tags=["Annotate Endpoint"],
    )
    @use_kwargs(annotateSchema)
    def post(self, **kwargs):
        
        text = kwargs["text"]
        
        properties = []
        if "properties" in kwargs:
            properties = kwargs["properties"]
        
        filter_value = 0.0
        if "filter_value" in kwargs:
            filter_value = kwargs["filter_value"]

        text = tika_helper.prepare_text(text)

        sources = config_helper.app_config["sources"]

        similar_documents = seta_helper.get_similar_docs_by_content(text, 10, sources)

        if not similar_documents:
            return {"message": "No similar documents found"}, 404

        supported_properties = {}
        
        if len(properties) == 0:
            properties = config_helper.app_config["supported_properties"]
        
        for property in properties:
            supported_properties[property] = defaultdict(list)

        response = defaultdict(list)
        
        metadata = metadata_helper.extract_from_docs(
            similar_documents["documents"], supported_properties, filter_value
        )
        
        labels = self.get_labels(metadata)

        metadata = self.prepare_metadata_response(metadata,labels)

        similar_documents_response = self.prepare_similar_documents_response(similar_documents)
        
        response["similar_docs"] = similar_documents_response
        response["metadata"] = metadata

        return response


    def prepare_similar_documents_response(self, similar_documents):
        similar_documents_response = []
        for doc in similar_documents["documents"]:
            similar_documents_response.append({"id": doc["id"], "title": doc["title"]})
        return similar_documents_response


    def get_labels(self,metadata):
        eurovoc_concepts = []
        
        for key in metadata:
            eurovoc_concepts.extend([tup[0] for tup in metadata[key]])
        
        sparql = sparql_op_helper.init_sparql_endpoint()

        labels = (
            sparql_op_helper.get_metadata_labels_from_cellar_sparql_endpoint2(
                eurovoc_concepts, sparql
            )
        )
        return labels

    def prepare_metadata_response(self,metadata, labels):
        """
        return medatadata in the following format:
        id, label, score
        """
        # convert tuples to key, value pairs
        
        metadata_response = {}

        for key, value in metadata.items():
            new_value = []
            for item in value:
                id = item[0].split("/")[-1]
                url = item[0]
                new_item = {"id": id,
                            "label": labels[url],
                            "url": url , 
                            "score": item[1]
                            }
                new_value.append(new_item)
            metadata_response[key] = new_value

        return metadata_response

def init_list_of_supported_properties():
    global supported_properties
    properties = config_helper.app_config["supported_properties"]
    supported_properties = {}

    for property in properties:
        supported_properties[property] = defaultdict(list)
    return supported_properties
